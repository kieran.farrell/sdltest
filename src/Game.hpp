#ifndef GAME_H
#define GAME_H
#pragma once

#include <iostream>
#include <string>
#include <SDL.h>
#include <vector>

#include "Entity.hpp"
#include "Level.hpp"
	
class Game  
{
public:
	Game(SDL_Rect position, const std::string& title);
	~Game();

	void run();

	void handleEvents();
	void update();
	void render();



private:
	SDL_Window* m_window;
	SDL_Renderer* m_renderer;
	bool m_running;

	const int SCREEN_FPS = 60;

	Entity *m_player = nullptr;
	Entity *m_monster = nullptr;
	Level m_level;
	

};
#endif