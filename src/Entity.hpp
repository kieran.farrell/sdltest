#ifndef ENTITY_H
#define ENTITY_H
#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
	
class Entity  
{
	private:
		SDL_Rect m_position = {0};
		SDL_Surface* m_surface = nullptr;
		SDL_Rect  m_prevPosition = {0};

	public:

		explicit Entity(SDL_Rect position);
		Entity(SDL_Rect position, const std::string& asset);
		~Entity();

		void draw(SDL_Renderer* renderer);
		void move(SDL_Rect position);

		SDL_Rect getPosition() { return m_position; }
		void setPosition(SDL_Rect position) { m_position = position; }

		SDL_Rect getPrevPosition() { return m_prevPosition; }
		void setPrevPosition(SDL_Rect position) { m_prevPosition = position; }

		// check collision with another entity
		SDL_Rect checkCollision(Entity *other);

};
#endif