#ifndef LEVEL_H
#define LEVEL_H
#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <array>
#include <SDL.h>

#include "Entity.hpp"
	

class Level  
{

public:
	Level();
	~Level();

	void update();
    auto getEntities() { return m_entities; }


private:
    std::vector<Entity> m_entities;
};
	

#endif