#include <SDL.h>
#include "Game.hpp"

int main() try
{
	Game game = Game({100, 100, 640, 480}, "Test");
	game.run();
}
catch (const std::exception& e)
{
	std::cerr << e.what() << std::endl;
	return EXIT_FAILURE;
}

