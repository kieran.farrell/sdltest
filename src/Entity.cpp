#include "Entity.hpp"  
	
Entity::Entity(SDL_Rect position)
: m_position(position)
{

}

Entity::Entity(SDL_Rect position, const std::string& asset)
: m_position(position)
{
    m_surface = IMG_Load(asset.c_str());
    if(m_surface == nullptr)
    {
        throw std::runtime_error("Error loading image");
    }

}

	
Entity::~Entity() = default;

void Entity::draw(SDL_Renderer* renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    
    if(!m_surface)
    {   
        SDL_RenderFillRect(renderer, &m_position);
    }
    else
    {
        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, m_surface);
        SDL_RenderCopy(renderer, texture, nullptr, &m_position);
        SDL_DestroyTexture(texture);
    }

}

void Entity::move(SDL_Rect position)
{
    m_position.x += position.x;
    m_position.y += position.y;
}

SDL_Rect Entity::checkCollision(Entity *other)
{
    // SDL Intersection
    if(SDL_HasIntersection(&m_position, &other->m_position))
    {
        SDL_Rect collision;
        collision.x = m_position.x - other->m_position.x;
        collision.y = m_position.y - other->m_position.y;
        return collision;
    }
    else
    {
        SDL_Rect collision;
        collision.x = 0;
        collision.y = 0;
        return collision;
    }
}