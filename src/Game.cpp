#include "Game.hpp"  
	
Game::Game(SDL_Rect position, const std::string& title)
{
	SDL_Init(SDL_INIT_EVERYTHING);
    m_window = SDL_CreateWindow(title.c_str(), position.x, position.y, position.w, position.h, SDL_WINDOW_OPENGL);
	if(m_window == nullptr)
	{
		throw std::runtime_error("Window could not be created!");
	}


    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
	if(m_renderer == nullptr)
	{
		throw std::runtime_error("Renderer could not be created!");
	}
	
	std::cout << "Game created" << std::endl;
	m_running = true;

    m_player = new Entity({100, 100, 32, 32}, "../assets/willyl0.png");
	std::cout << "Entity created" << std::endl;

    m_monster = new Entity({150, 150, 32, 32});
	std::cout << "Monster created" << std::endl;
	
}
	
Game::~Game()
{
	if(m_player != nullptr)
	{
		delete m_player;
		std::cout << "Entity destroyed" << std::endl;
	}
	
	
	if(m_monster != nullptr)
	{
		delete m_monster;
		std::cout << "Monster destroyed" << std::endl;
	}
	
    SDL_DestroyWindow(m_window);
    SDL_DestroyRenderer(m_renderer);
    SDL_Quit();

    std::cout << "Game destroyed" << std::endl;
}

void Game::handleEvents()
{
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT:
                m_running = false;
				break;
			
			case SDL_KEYDOWN:
				m_player->setPrevPosition(m_player->getPosition());
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						m_player->move({0, -5});
						break;
					case SDLK_DOWN:
						m_player->move({0, 5});
						break;
					case SDLK_LEFT:
						m_player->move({-5, 0});
						break;
					case SDLK_RIGHT:
						m_player->move({5, 0});
						break;
					case SDLK_ESCAPE:
                        m_running = false;
						break;
				}
			default:
				break;
		}
	}
}

void Game::update()
{
	// Check collisions
	SDL_Rect collisionBox = m_player->checkCollision(m_monster);
	if(collisionBox.x && collisionBox.y)
	{
		m_player->setPosition(m_player->getPrevPosition());
	}
	else
	{
		//std::cout << "No collision" << std::endl;		
	}

	for(auto i : m_level.getEntities())
	{
        collisionBox = m_player->checkCollision(&i);
		
		if(collisionBox.x && collisionBox.y)
		{
			m_player->setPosition(m_player->getPrevPosition());
			break;
		}
		else
		{
			//std::cout << "No collision" << std::endl;		
		}
	}


    m_level.update();

}

void Game::render()
{
	// Clear background
	SDL_SetRenderDrawColor(m_renderer, 255, 0, 0, 255);
	SDL_RenderClear(m_renderer);


	// Draw Here
	SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);
	for(auto i : m_level.getEntities())
	{
		i.draw(m_renderer);
	}

	m_player->draw(m_renderer);
	m_monster->draw(m_renderer);


	// Update screen
	SDL_RenderPresent(m_renderer);
}

void Game::run()
{
	const int frameDelay = 1000 / SCREEN_FPS;
	Uint32 frameStart;
	Uint32 frameTime;

	while(m_running)
	{
		frameStart = SDL_GetTicks();

		handleEvents();
		update();
		render();

		frameTime = SDL_GetTicks() - frameStart;
		if(frameDelay > frameTime)
		{
			SDL_Delay(frameDelay - frameTime);
		}

	}

}


